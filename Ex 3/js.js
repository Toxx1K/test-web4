
$("#submit_form_ticket").click(function(e) {
    e.preventDefault();
    addTicket();
});

/**
 * Permet l'ajout d'un ticket
 * @param none
 * @return none, have response string json
 */
function addTicket() {

    var data = $("#form_ticket").serialize();
    $.ajax({
        cache: false,
        url: "addTicket.php",
        data: data,
        dataType: "json",
        method: "post",
        success: function(response) {
            console.log(response.status);
            // On imagine tout type d'action ici, comme afficher un message de confirmation, une redirection etc etc .
        }
    });
}