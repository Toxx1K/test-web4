<!DOCTYPE html>
<html>
<head>
  <title>Liste billets</title>
   <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Bootstrap -->
  <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
</head>
<body>


  <div class="navbar">
    <div class="navbar-inner">
      <div class="container">

        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>
        <a class="brand" href="#">WEB4</a>

        <div class="nav-collapse collapse">
          <ul class="nav">
            <li>
              <a href="#">Accueil</a>
            </li>
            <li class="active"><a href="#">Billets</a></li>
            <li><a href="#">Un lien</a></li>
          </ul>
        </div>

      </div>
    </div>
  </div>


  <div class="container">

    <div class="row">

    <div class="span8">

      <h2 class="page-header">Billet No. 1</h2>
        <form class="form-horizontal">

            <div class="control-group">
            <label class="control-label" for="id">ID</label>
            <div class="controls">
              <input type="text" name="id" value ="1" disabled=disabled>
            </div>
          </div>

          <div class="control-group">
            <label class="control-label" for="name_cli">Nom client</label>
            <div class="controls">
              <select name="name_cli" id="">
                <option value="">Pré rempli par bdd</option>
              </select>
            </div>
          </div>

          <div class="control-group">
            <label class="control-label" for="spectacle_id">Spectacle</label>
            <div class="controls">
              <select name="spectacle_id" id="">
                <option value="">
                  Pré rempli par bdd
                </option>
              </select>
            </div>
          </div>

          <div class="control-group">
            <label class="control-label" for="date">Date</label>
            <div class="controls">
              <input type="text" id="date" placeholder="Date" value="Pré rempli par bdd">
            </div>
          </div>

          <div class="control-group">
            <label class="control-label" for="barecode">Code barre</label>
            <div class="controls">
              <input type="text" id="barecode" placeholder="Code barre" value="Pré rempli par bdd">
            </div>
          </div>


          <div class="control-group">
            <div class="controls">
              <button type="submit" class="btn btn-success">Modifier</button>
              <button type="submit" class="btn btn-warning">Supprimer</button>
              <button type="submit" class="btn btn-info">Retour</button>
            </div>
          </div>
        </form>
      </div>

    </div>



    </div>

  </div>

  <script src="http://code.jquery.com/jquery.js"></script>
  <script src="js/bootstrap.min.js"></script>
</body>
</html>