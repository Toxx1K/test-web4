<!DOCTYPE html>
<html>
<head>
  <title>Liste billets</title>
   <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Bootstrap -->
  <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
</head>
<body>


  <div class="navbar">
    <div class="navbar-inner">
      <div class="container">

        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>
        <a class="brand" href="#">WEB4</a>

        <div class="nav-collapse collapse">
          <ul class="nav">
            <li>
              <a href="#">Accueil</a>
            </li>
            <li class="active"><a href="#">Billets</a></li>
            <li><a href="#">Un lien</a></li>
          </ul>
        </div>

      </div>
    </div>
  </div>


  <div class="container">

    <div class="row">
      <div class="span8" id="ticket-list">
       <h2 class="page-header">Liste des billets</h2>
       <table class="table table-striped">
        <thead>
          <th>ID</th>
          <th>Nom client</th>
          <th>Spectacle </th>
          <th>Date </th>
          <th>Code barre</th>
          <th></th>
          <th></th>
        </thead>
        <tbody>
          <?php //simple script php de remplissage

          for($i=1 ; $i<=10 ; $i++){ ?>

          <tr>
            <td><?php echo $i; ?></td>
            <td>Nom client</td>
            <td>Spectacle</td>
            <td>Date</td>
            <td>Code barre</td>
            <td><a href="viewTicket.php?id=<?php echo $i;?>"><button class="btn btn-info"><i class="fa fa-pencil"></i> Modifier</button></a></td>
            <td><button class="btn btn-warning"><i class="fa fa-times"></i></button></td>
          </tr>

          <?php   }   ?>
        </tbody>
      </table>
    </div>

    <div class="span4" id="sidebar">
      <h2 class="page-header">Autres actions</h2>
      <div id="addTicket">
        <h4>Ajouter un billet</h4>
        <form class="form-horizontal">



          <div class="control-group">
            <label class="control-label" for="name_cli">Nom client</label>
            <div class="controls">
              <select name="name_cli" id=""></select>
            </div>
          </div>

          <div class="control-group">
            <label class="control-label" for="spectacle_id">Spectacle</label>
            <div class="controls">
              <select name="spectacle_id" id=""></select>
            </div>
          </div>

          <div class="control-group">
            <label class="control-label" for="date">Date</label>
            <div class="controls">
              <input type="text" id="date" placeholder="Date">
            </div>
          </div>

          <div class="control-group">
            <label class="control-label" for="barecode">Code barre</label>
            <div class="controls">
              <input type="text" id="barecode" placeholder="Code barre">
            </div>
          </div>


          <div class="control-group">
            <div class="controls">
              <button type="submit" class="btn">OK</button>
            </div>
          </div>
        </form>
      </div>


    </div>

  </div>

  <script src="http://code.jquery.com/jquery.js"></script>
  <script src="js/bootstrap.min.js"></script>
</body>
</html>