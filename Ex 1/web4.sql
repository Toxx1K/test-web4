
DROP TABLE IF EXISTS `client`;

CREATE TABLE `client` (
  `id_client` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nom_client` varchar(50) DEFAULT NULL,
  `prenom_client` varchar(50) DEFAULT NULL,
  `email_client` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_client`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `spectacle`;

CREATE TABLE `spectacle` (
  `id_spectacle` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nom_spectacle` varchar(50) DEFAULT NULL,
  `date_spectacle` date DEFAULT NULL,
  `heure_spectacle` int(11) DEFAULT NULL,
  `lieu_spectacle` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_spectacle`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `billet`;

CREATE TABLE `billet` (
  `id_billet` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `barecode` int(12) DEFAULT NULL,
  `id_client` int(11) unsigned NOT NULL,
  `id_spectacle` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_billet`),
  KEY `fk_client` (`id_client`),
  KEY `fk_spectacle` (`id_spectacle`),
  CONSTRAINT `fk_spectacle` FOREIGN KEY (`id_spectacle`) REFERENCES `spectacle` (`id_spectacle`) ,
  CONSTRAINT `fk_client` FOREIGN KEY (`id_client`) REFERENCES `client` (`id_client`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


