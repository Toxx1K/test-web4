<?php

/**
 * Classe gérant l'ensemble des fonctionnalités de la base de données
 *
 * @package    PackageName
 * @author J-Louis
 */
class database extends PDO {

    /**
     * Crée une connexion à la base de données
     *
     * @param string $hostname nom du server
     * @param string $dbname  nom de la base de donnée
     * @param string $username login
     * @param string $password mot de passe de la base de donnée
     *
     * @return  void
     */
    public function __construct($hostname, $dbname, $username, $password) {

    }

    /**
     * Renvoie le nombre de résultat de la reqûete
     *
     * @param string $query la chaîne de la requête
     * @return  void
     */
    public function num_rows($query) {

    }

    /**
     * Renvoie les erreurs de la base de données
     *
     * @param  void
     * @return void
     */
    public function get_error() {

    }

    /**
     * Détruit l'instance de la bdd
     *
     * @param  void
     * @return void
     */
    public function __destruct() {

    }

    /**
     * Retourne l'instance de la connexion
     *
     * @param  void
     * @return Object database
     */
    public static function instance() {

    }

    /**
     * Execute la requête
     *
     * @param string $sql Chaîne contenant la requête à effectuer
     * @return mixed resultat de la requête
     */
    public function query($sql) {

    }

}
