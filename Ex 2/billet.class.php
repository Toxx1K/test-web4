<?php

/**
 * Classe gérant l'ensemble des fonctionnalités des billets
 *
 * @package    PackageName
 * @author J-Louis
 */
class Billets {

    /**
     * Données de l'objet
     * @var array
     */
    private $data = array();

    /**
     *  Objet de base de données
     * @var database
     */
    private $db;

    /**
     * Contructeur de la classe
     *
     * @param array $billet Tableau contenant les données de l'objet
     * @return  void
     */
    public function __construct($billet) {
        if (!empty($billet)) {
            $this->data = $billet;
        }
        $this->db = new database('host', 'dbname', 'user', 'pwd');
    }

    /**
     * Setter commun à la classe
     *
     * @param string $name Nom de l'attribut
     * @param mixed $value Valeur de l'attribut
     *
     * @return void
     */
    public function __set($name, $value) {
        $this->data[$name] = $value;
    }

    /**
     * Getter commun à la classe
     *
     * @param  string $name Attribut à récupérer
     * @return mixed attributs
     */
    public function __get($name) {
        if (array_key_exists($name, $this->data)) {
            return $this->data[$name];
        }
    }

    /**
     * Renvoie tous les tickets d'un client en particulier
     *
     * @param  string $id_client Id du client voulu
     * @return Array Object Billet
     */
    public function get_tickets_from_client($id_client) {
        $result = $this->db->query("SELECT * FROM billet WHERE id_client = " . $id_client);
        $tickets = array();
        while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
            $tickets[] = new Billets($row);
        }
        return $tickets;
    }

    /**
     * Ajoute un ticket à la bdd
     *
     * @param Billet $ticket Objet billet à ajouter à la bdd
     * @return void
     */
    public function add_ticket($ticket) {
        $sql = "INSERT INTO billet ('barecode_billet','id_client','id_spectacle') VALUES ('" . $ticket->barecode_billet . "','" . $ticket->id_client . "','" . $ticket->id_spectacle . "')";
        $this->db->exec($sql);
    }

    /**
     * Récupère un billet selon l'id
     *
     * @param  string $id Id du billet à récupérer
     * @return Object Billet
     */
    public function get_ticket($id) {
        $result = $this->db->query("SELECT * FROM billet WHERE id_billet = " . $id);
        $row = $result->fetch(PDO::FETCH_ASSOC);
        $ticket = new Billet($row);
        return $ticket;
    }

}
